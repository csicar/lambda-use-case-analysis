

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from textwrap import wrap
import sqlite3
from IPython.display import JSON
import matplotlib.ticker as mtick
```


```python
conn = sqlite3.connect("../github-lambda-java/database.sqlite")
pd.read_sql_query("SELECT * FROM LambdaInfo LIMIT 5", conn).head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>clazz</th>
      <th>name</th>
      <th>type</th>
      <th>outside_references</th>
      <th>file</th>
      <th>side_effecting</th>
      <th>file_line</th>
      <th>source_text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=validate)</td>
      <td>validate</td>
      <td>0</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2428</td>
      <td>Validator.of(sarah).validate(User::getName, Ob...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=validate)</td>
      <td>validate</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2348</td>
      <td>Validator.of(sarah).validate(User::getName, Ob...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=validate)</td>
      <td>validate</td>
      <td>0</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>NO</td>
      <td>2278</td>
      <td>Validator.of(sarah).validate(User::getName, Ob...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=assertThrows)</td>
      <td>assertThrows</td>
      <td>6</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1808</td>
      <td>assertThrows(IllegalStateException.class, ()-&gt;...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=assertThrows)</td>
      <td>assertThrows</td>
      <td>4</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1537</td>
      <td>assertThrows(IllegalStateException.class, ()-&gt;...</td>
    </tr>
  </tbody>
</table>
</div>




```python
df = pd.read_sql_query("""SELECT clazz, type, name, refs, count_name FROM 
    (
        SELECT count(*) as count_name, avg(outside_references) as refs,  * 
        FROM LambdaInfo 
        Where not (file like "%/test/%" or file like "%/tests/%") 
        GROUP BY name 
        ORDER BY count_name DESC
    )""", conn)
df.head(7)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>clazz</th>
      <th>type</th>
      <th>name</th>
      <th>refs</th>
      <th>count_name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>map</td>
      <td>InvocationFieldContext(name=map)</td>
      <td>2.535116</td>
      <td>6507</td>
    </tr>
    <tr>
      <th>1</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>forEach</td>
      <td>InvocationFieldContext(name=forEach)</td>
      <td>5.329598</td>
      <td>6068</td>
    </tr>
    <tr>
      <th>2</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>filter</td>
      <td>InvocationFieldContext(name=filter)</td>
      <td>1.196473</td>
      <td>5217</td>
    </tr>
    <tr>
      <th>3</th>
      <td>evaluation.ReturnContext</td>
      <td>RETURN</td>
      <td>ReturnContext(type=RETURN)</td>
      <td>6.861244</td>
      <td>3344</td>
    </tr>
    <tr>
      <th>4</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>computeIfAbsent</td>
      <td>InvocationFieldContext(name=computeIfAbsent)</td>
      <td>3.613960</td>
      <td>1404</td>
    </tr>
    <tr>
      <th>5</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>ifPresent</td>
      <td>InvocationFieldContext(name=ifPresent)</td>
      <td>3.322373</td>
      <td>1399</td>
    </tr>
    <tr>
      <th>6</th>
      <td>evaluation.InvocationFieldContext</td>
      <td>put</td>
      <td>InvocationFieldContext(name=put)</td>
      <td>2.078032</td>
      <td>1179</td>
    </tr>
  </tbody>
</table>
</div>




```python
totalNumberOfLambdas = pd.read_sql_query("SELECT count(*) as c FROM LambdaInfo ", conn)['c'][0]
totalNumberOfLambdas
```




    151480



total number of Lambdas found


```python
normalLambdaCount = pd.read_sql_query("""
    SELECT count(*) as c FROM LambdaInfo Where not (file like "%/test/%" or file like "%/tests/%") """, conn)['c'][0]
normalLambdaCount
```




    73496



number of lambdas that are not in tests


```python
javaFileCount = pd.read_sql_query("""
    SELECT count(*) as c, file FROM LambdaInfo GROUP BY file ORDER BY c DESC""", conn)
javaFileCount['file'].count()
```




    37355




```python
javaFileCount = pd.read_sql_query("""
    SELECT count(*) as c, file FROM LambdaInfo WHERE NOT (file LIKE "%/test/%" or file LIKE "%/tests/%") GROUP BY file ORDER BY c DESC""", conn)
javaFileCount['file'].count()
```




    22991



#  How are the uses distributed?


```python
lambdasByCoarseCategory = pd.read_sql_query("""
    SELECT *, count(*) as lambda_count
    FROM LambdaInfo
    WHERE NOT (file LIKE "%/test/%" OR file LIKE "%/tests/%") 
    GROUP BY clazz
    ORDER BY lambda_count DESC""", conn)

nice_clazz_name_map = {
    "evaluation.InvocationFieldContext": "Invocation\n of a Method\n by Field",
    "evaluation.InvocationContext": "other\n Invocation",
    "evaluation.OtherContext": "any other\n Context",
    "evaluation.ReturnContext": "return\n statement",
    "evaluation.NewClassContext": "constructor\n call",
    "evaluation.AssignContext": "assignment"
}

nice_clazz_names = map(lambda clazz: nice_clazz_name_map[clazz], lambdasByCoarseCategory['clazz'])


plt.rcParams.update({'font.size': 18})



plt.figure(figsize=(12, 7))
plt.bar(np.arange(6), 100*lambdasByCoarseCategory['lambda_count'] / normalLambdaCount)

plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter())
plt.ylabel("[%] of total number of lambdas\n (excluding tests)", labelpad=10)
plt.xlabel("Context lambda expressions were found in ", labelpad=25)
plt.xticks(np.arange(6), nice_clazz_names)
for tick in plt.gca().xaxis.get_major_ticks()[0:]:
    tick.set_pad(10)

plt.tight_layout()
plt.savefig("coarse_category.png", pad_inches=-10)
plt.show()

```


![png](output_10_0.png)



```python
numOfElements = 15

def firsts(col):
    return col[0:numOfElements]

indices = np.arange(numOfElements+1)
types = pd.concat([   firsts(df["type"]), pd.Series(["others"])   ])
counts = pd.concat([  firsts(df["count_name"]), pd.Series([ df["count_name"][numOfElements:].sum()  ]) ])
colorCategory = {
    "Stream": ("tab:blue", ["map", "forEach", "filter", "flatMap", "anyMatch", "toMap"]),
    "Runnable / Listener": ("tab:red", ["subscribe", "setOnClickListener", "execute", "submit"]),
    "Optional": ("tab:orange", ["computeIfAbsent", "ifPresent", "orElseThrow"]),
    "Other": ("tab:grey", ["others", "RETURN", "put"]),
}
colors = [ ([ colorCategory[name][0] for name in colorCategory if t in colorCategory[name][1] ] + ["tab:grey"])[0] for t in types ]
plt.figure(figsize=(10, 8))
plt.rcParams.update({'font.size': 18})

plt.grid(alpha=0.3)

for category in colorCategory:
    methodNames = colorCategory[category][1]
    countIfInCategory = [count if tt in methodNames else 10 for (count, tt) in zip(counts, types)]
    ax = plt.bar(
        indices,
        100 * np.array(countIfInCategory) / normalLambdaCount, color=colorCategory[category][0])


plt.legend([categoryName for categoryName in colorCategory])

plt.ylabel("[%] of total number of Lambdas\n (excluding tests)")
plt.xlabel("Name of Method-Invocation", labelpad=25)
plt.xticks(indices, types, rotation='vertical')
plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter())
plt.tight_layout()
plt.savefig("fine_category.png")
plt.show()
```


![png](output_11_0.png)


# How relevant are the use-cases that Cok identified in AWS?

## Functions as arguments


```python
lambdaInArgument = pd.read_sql_query("""
    SELECT * FROM LambdaInfo
    WHERE NOT (file LIKE "%/test/%" OR file LIKE "%/tests/%") 
    AND clazz like "evaluation.Invocation%" """, conn)
lambdaInArgument['clazz'].count() / normalLambdaCount
```




    0.8315282464351802



## Functions as variables


```python
df[(df['clazz'] == "evaluation.AssignContext") | (df['type'] == "VARIABLE")]['count_name'].sum() / normalLambdaCount
```




    0.05837052356590835



number of lambda-expressions that were found in a assign-context

## Lambdas in Streams


```python
lambdaStreamUses = pd.read_sql_query("""
    SELECT * FROM LambdaInfo
    
    WHERE NOT (file LIKE "%/test/%" OR file LIKE "%/tests/%") 
    AND source_text LIKE "%.stream()%" OR source_text LIKE "%Collections.%" """, conn)
lambdaStreamUses['clazz'].count() / normalLambdaCount
```




    0.18373789049744205



## How many uses of FP in methods?


```python
methodCount = pd.read_sql_query("""
    SELECT count(*) as c FROM MethodInfo""", conn)['c'][0]
methodCount
```




    5334726




```python
pd.read_sql_query("""SELECT * FROM MethodInfo WHERE NOT name LIKE "%test%" ORDER BY number_lines DESC LIMIT 5 """, conn)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>number_fp_features</th>
      <th>number_lines</th>
      <th>name</th>
      <th>source_text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5154022</td>
      <td>0</td>
      <td>30379</td>
      <td>foo</td>
      <td>\nprotected void foo() {\n    int j = 4;\n    ...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2562979</td>
      <td>0</td>
      <td>5231</td>
      <td>jjMoveNfa_0</td>
      <td>\nprivate int jjMoveNfa_0(int startState, int ...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3081717</td>
      <td>0</td>
      <td>4231</td>
      <td>main</td>
      <td>\npublic static void main(String[] args) throw...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3081738</td>
      <td>0</td>
      <td>4231</td>
      <td>main</td>
      <td>\npublic static void main(String[] args) throw...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1781328</td>
      <td>0</td>
      <td>3187</td>
      <td>next</td>
      <td>\npublic String next() throws java.io.IOExcept...</td>
    </tr>
  </tbody>
</table>
</div>




```python
fpMethodUses = pd.read_sql_query("""
    SELECT 
        avg(number_fp_features) as avg_fp_features,
        count(*) as number_of_methods,
        avg(number_lines) as avg_number_lines 
    FROM MethodInfo
    GROUP BY number_fp_features > 0
    """, conn)
fpMethodUses.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>avg_fp_features</th>
      <th>number_of_methods</th>
      <th>avg_number_lines</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.000000</td>
      <td>5219700</td>
      <td>8.624175</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.820075</td>
      <td>115026</td>
      <td>16.195173</td>
    </tr>
  </tbody>
</table>
</div>




```python
100* fpMethodUses['number_of_methods'][1] / methodCount
```




    2.1561744689417974



% of methods have FP features


```python
def groupFP(rawdata):
    max_lines = 30#rawdata["number_lines"].max()
    data = rawdata.groupby(pd.cut(rawdata["number_lines"], np.arange(0, max_lines, 2))).sum()
    
    return data
   

fpLength = pd.read_sql_query("""
    SELECT
        count(*) as count_methods,
        number_lines,
        avg(number_fp_features) as avg_fp_features
    FROM MethodInfo
    WHERE number_fp_features > 0
    GROUP BY number_lines
    ORDER BY number_lines
    """, conn)


fpLengthGrouped = groupFP(fpLength)
```


```python
nonFpLength = pd.read_sql_query("""
    SELECT
        count(*) as count_methods,
        number_lines,
        avg(number_fp_features) as avg_fp_features
    FROM MethodInfo
    WHERE number_fp_features == 0
    GROUP BY number_lines
    ORDER BY number_lines
    """, conn)

def relativeData(data):
    return 100*data / data.sum()

nonFpLengthGrouped = groupFP(nonFpLength)

indices = np.arange(fpLengthGrouped["count_methods"].count())
plt.figure(figsize=(20, 10))
#plt.gca().set_yscale("log")
plt.bar(indices+0.25, relativeData(fpLengthGrouped["count_methods"]), 0.5)
plt.bar(indices-0.25, relativeData(nonFpLengthGrouped["count_methods"]), 0.5)

plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter())
plt.xticks(indices, fpLengthGrouped.index.values,  rotation='vertical')
plt.ylabel("[%] number of methods with(out) FP features\n relative to total number of methods\n with FP and non FP respectively")
plt.xlabel("Method's lines of code")
plt.legend(('FP', 'non FP'))
plt.show()
```


![png](output_27_0.png)


# How long are Lambda Expression bodys?


```python
sources = pd.read_sql_query("""SELECT source_text as lines FROM LambdaInfo 
        Where not (file like "%/test/%" or file like "%/tests/%") 
     """, conn)

```


```python
def groupLines(rawdata):
    max_lines = 36
    max_value = rawdata["lines"].max()
    indices = np.concatenate(([0], np.arange(1, max_lines, 5), [max_value]))
    data = rawdata.groupby(pd.cut(rawdata["lines"], indices)).sum()
    
    return data

lambdalinesToCount = groupLines(sources.applymap(lambda x: x.count("\n")+1).groupby("lines").size().reset_index(name='count'))
lambdalinesToCount['count'] = 100*lambdalinesToCount['count'] / normalLambdaCount

lambdalinesToCount.head(30)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>lines</th>
      <th>count</th>
    </tr>
    <tr>
      <th>lines</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>(0, 1]</th>
      <td>1</td>
      <td>64.520518</td>
    </tr>
    <tr>
      <th>(1, 6]</th>
      <td>20</td>
      <td>17.490748</td>
    </tr>
    <tr>
      <th>(6, 11]</th>
      <td>45</td>
      <td>10.384239</td>
    </tr>
    <tr>
      <th>(11, 16]</th>
      <td>70</td>
      <td>3.475019</td>
    </tr>
    <tr>
      <th>(16, 21]</th>
      <td>95</td>
      <td>1.674921</td>
    </tr>
    <tr>
      <th>(21, 26]</th>
      <td>120</td>
      <td>0.815010</td>
    </tr>
    <tr>
      <th>(26, 31]</th>
      <td>145</td>
      <td>0.522477</td>
    </tr>
    <tr>
      <th>(31, 579]</th>
      <td>13250</td>
      <td>1.117068</td>
    </tr>
  </tbody>
</table>
</div>




```python
plt.figure(figsize=(14, 8))
plt.rcParams.update({'font.size': 18})


#plt.gca().set_yscale("log")

max_lines = 1000
indices = np.arange(len(lambdalinesToCount.index))


plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter())
plt.ylabel("[%] percentage of lambda\n expressions with length")
plt.xlabel("Number of Lines")

plt.grid(alpha=0.3)


plt.xticks(indices, lambdalinesToCount.index.values)


plt.tight_layout()
plt.bar(indices, lambdalinesToCount['count'])
plt.savefig("lambda_loc.png")

```


![png](output_31_0.png)


# How is "execute" used?


```python
pd.read_sql_query("""SELECT *
        FROM LambdaInfo 
        Where not (file like "%/test/%" or file like "%/tests/%") 
        AND type IS "execute"
        """, conn)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>clazz</th>
      <th>name</th>
      <th>type</th>
      <th>outside_references</th>
      <th>file</th>
      <th>side_effecting</th>
      <th>file_line</th>
      <th>source_text</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>100</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2633</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>101</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2214</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>139</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2681</td>
      <td>executorService.execute(()-&gt;makeServiceCalls(n...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>140</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2608</td>
      <td>executorService.execute(()-&gt;makeServiceCalls(a...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>149</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2406</td>
      <td>executorService.execute(()-&gt;{\n    while (inve...</td>
    </tr>
    <tr>
      <th>5</th>
      <td>162</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>5</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3493</td>
      <td>executor.execute(()-&gt;{\n    try {\n        ful...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>190</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>5</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3979</td>
      <td>reactorMain.execute(()-&gt;{\n    try {\n        ...</td>
    </tr>
    <tr>
      <th>7</th>
      <td>191</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>4</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2338</td>
      <td>executorService.execute(()-&gt;channel.getHandler...</td>
    </tr>
    <tr>
      <th>8</th>
      <td>546</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2181</td>
      <td>execute((fileContext1)-&gt;fileContext1.makeQuali...</td>
    </tr>
    <tr>
      <th>9</th>
      <td>555</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>4</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>5340</td>
      <td>store.execute((fileContext)-&gt;(fileContext.util...</td>
    </tr>
    <tr>
      <th>10</th>
      <td>557</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>6</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3368</td>
      <td>store.execute((fileContext)-&gt;new HDFSPrivilege...</td>
    </tr>
    <tr>
      <th>11</th>
      <td>558</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2640</td>
      <td>store.execute((fileContext)-&gt;fileContext.delet...</td>
    </tr>
    <tr>
      <th>12</th>
      <td>559</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2373</td>
      <td>store.execute((fileContext)-&gt;fileContext.util(...</td>
    </tr>
    <tr>
      <th>13</th>
      <td>919</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2633</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>14</th>
      <td>920</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2214</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>15</th>
      <td>958</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2681</td>
      <td>executorService.execute(()-&gt;makeServiceCalls(n...</td>
    </tr>
    <tr>
      <th>16</th>
      <td>959</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2608</td>
      <td>executorService.execute(()-&gt;makeServiceCalls(a...</td>
    </tr>
    <tr>
      <th>17</th>
      <td>968</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2406</td>
      <td>executorService.execute(()-&gt;{\n    while (inve...</td>
    </tr>
    <tr>
      <th>18</th>
      <td>981</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>5</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3493</td>
      <td>executor.execute(()-&gt;{\n    try {\n        ful...</td>
    </tr>
    <tr>
      <th>19</th>
      <td>1009</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>5</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3979</td>
      <td>reactorMain.execute(()-&gt;{\n    try {\n        ...</td>
    </tr>
    <tr>
      <th>20</th>
      <td>1010</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>4</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2338</td>
      <td>executorService.execute(()-&gt;channel.getHandler...</td>
    </tr>
    <tr>
      <th>21</th>
      <td>1365</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2181</td>
      <td>execute((fileContext1)-&gt;fileContext1.makeQuali...</td>
    </tr>
    <tr>
      <th>22</th>
      <td>1374</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>4</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>5340</td>
      <td>store.execute((fileContext)-&gt;(fileContext.util...</td>
    </tr>
    <tr>
      <th>23</th>
      <td>1376</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>6</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3368</td>
      <td>store.execute((fileContext)-&gt;new HDFSPrivilege...</td>
    </tr>
    <tr>
      <th>24</th>
      <td>1377</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2640</td>
      <td>store.execute((fileContext)-&gt;fileContext.delet...</td>
    </tr>
    <tr>
      <th>25</th>
      <td>1378</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2373</td>
      <td>store.execute((fileContext)-&gt;fileContext.util(...</td>
    </tr>
    <tr>
      <th>26</th>
      <td>2637</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>9</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>6680</td>
      <td>executor.execute(()-&gt;{\n    try {\n        sta...</td>
    </tr>
    <tr>
      <th>27</th>
      <td>4611</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2633</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>28</th>
      <td>4612</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2214</td>
      <td>executorService.execute(()-&gt;{\n    guardedQueu...</td>
    </tr>
    <tr>
      <th>29</th>
      <td>4650</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2681</td>
      <td>executorService.execute(()-&gt;makeServiceCalls(n...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>819</th>
      <td>139909</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>6</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>12613</td>
      <td>BackendOperation.execute((txh)-&gt;{\n    idStore...</td>
    </tr>
    <tr>
      <th>820</th>
      <td>139949</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>44</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>8843</td>
      <td>BackendOperation.execute(()-&gt;{\n    final Stan...</td>
    </tr>
    <tr>
      <th>821</th>
      <td>139950</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>9</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>8180</td>
      <td>BackendOperation.execute(()-&gt;{\n    final Log ...</td>
    </tr>
    <tr>
      <th>822</th>
      <td>140967</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>34</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2063</td>
      <td>mDrawingThreadPool.execute(()-&gt;{\n    Canvas c...</td>
    </tr>
    <tr>
      <th>823</th>
      <td>141023</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>3</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2203</td>
      <td>mExecutor.execute(()-&gt;{\n    try {\n        pr...</td>
    </tr>
    <tr>
      <th>824</th>
      <td>141024</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>17</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1188</td>
      <td>mExecutor.execute(()-&gt;{\n    try {\n        pr...</td>
    </tr>
    <tr>
      <th>825</th>
      <td>141038</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>826</td>
      <td>mExecutorService.execute(()-&gt;splitWords(s.toSt...</td>
    </tr>
    <tr>
      <th>826</th>
      <td>141039</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>11</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3001</td>
      <td>mExecutorService.execute(()-&gt;{\n    List&lt;CodeC...</td>
    </tr>
    <tr>
      <th>827</th>
      <td>141071</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>11</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1457</td>
      <td>mDrawingThreadPool.execute(()-&gt;{\n    SurfaceH...</td>
    </tr>
    <tr>
      <th>828</th>
      <td>141123</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>9</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>2298</td>
      <td>mExecutorService.execute(()-&gt;{\n    try {\n   ...</td>
    </tr>
    <tr>
      <th>829</th>
      <td>141224</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>6</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3543</td>
      <td>mKeyEventExecutor.execute(()-&gt;{\n    stickOnKe...</td>
    </tr>
    <tr>
      <th>830</th>
      <td>141225</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>9</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1549</td>
      <td>mExecutor.execute(()-&gt;{\n    mDumping = true;\...</td>
    </tr>
    <tr>
      <th>831</th>
      <td>141269</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3697</td>
      <td>executor.execute(()-&gt;hmilyTransactionExecutor....</td>
    </tr>
    <tr>
      <th>832</th>
      <td>141270</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3400</td>
      <td>executor.execute(()-&gt;hmilyTransactionExecutor....</td>
    </tr>
    <tr>
      <th>833</th>
      <td>141272</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>20</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>YES</td>
      <td>1073</td>
      <td>executor.select(transId).execute(()-&gt;{\n    if...</td>
    </tr>
    <tr>
      <th>834</th>
      <td>147517</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2143</td>
      <td>conn.execute(sql, (res)-&gt;{\n    if (res.failed...</td>
    </tr>
    <tr>
      <th>835</th>
      <td>147522</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>2127</td>
      <td>conn.execute(sql, (res)-&gt;{\n    if (res.failed...</td>
    </tr>
    <tr>
      <th>836</th>
      <td>149706</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>11</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>3189</td>
      <td>executor().execute(()-&gt;{\n    fetchAndSendResu...</td>
    </tr>
    <tr>
      <th>837</th>
      <td>149805</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>440</td>
      <td>execute("composed CompletableFuture", ()-&gt;best...</td>
    </tr>
    <tr>
      <th>838</th>
      <td>149806</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>355</td>
      <td>execute("parallel", ()-&gt;bestPriceFinder.findPr...</td>
    </tr>
    <tr>
      <th>839</th>
      <td>149807</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>266</td>
      <td>execute("sequential", ()-&gt;bestPriceFinder.find...</td>
    </tr>
    <tr>
      <th>840</th>
      <td>149820</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>756</td>
      <td>execute("combined USD CompletableFuture v3", (...</td>
    </tr>
    <tr>
      <th>841</th>
      <td>149821</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>648</td>
      <td>execute("combined USD CompletableFuture v2", (...</td>
    </tr>
    <tr>
      <th>842</th>
      <td>149822</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>544</td>
      <td>execute("combined USD CompletableFuture", ()-&gt;...</td>
    </tr>
    <tr>
      <th>843</th>
      <td>149823</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>443</td>
      <td>execute("composed CompletableFuture", ()-&gt;best...</td>
    </tr>
    <tr>
      <th>844</th>
      <td>149824</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>358</td>
      <td>execute("parallel", ()-&gt;bestPriceFinder.findPr...</td>
    </tr>
    <tr>
      <th>845</th>
      <td>149825</td>
      <td>evaluation.InvocationContext</td>
      <td>InvocationContext(text=execute)</td>
      <td>execute</td>
      <td>1</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>269</td>
      <td>execute("sequential", ()-&gt;bestPriceFinder.find...</td>
    </tr>
    <tr>
      <th>846</th>
      <td>150212</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>7</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>4268</td>
      <td>exec.execute(()-&gt;{\n    try {\n        pushNex...</td>
    </tr>
    <tr>
      <th>847</th>
      <td>150380</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>2</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>4679</td>
      <td>_executorService.execute(()-&gt;warmUpServices(ca...</td>
    </tr>
    <tr>
      <th>848</th>
      <td>150672</td>
      <td>evaluation.InvocationFieldContext</td>
      <td>InvocationFieldContext(name=execute)</td>
      <td>execute</td>
      <td>8</td>
      <td>/home/csicar/data/Uni/BachelorArbeit/github-la...</td>
      <td>UNSURE</td>
      <td>1942</td>
      <td>_executor.execute(()-&gt;{\n    TransportCallback...</td>
    </tr>
  </tbody>
</table>
<p>849 rows × 9 columns</p>
</div>




```python

```


```python

```


```python

```
